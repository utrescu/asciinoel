package net.xaviersala.AsciiNoel;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class App {
    private static final String PARE_NOEL = "Pare Noel";
    private static final String REN_NOM = "Ren";
    private static final String FOLLET_NOM = "Follet";
    private static final String PARE = "\\*<]:-DOo";
    private static final String REN = ">:o\\)";
    private static final String FOLLET = "[^\\*]<]:-D";

    public static void main(String[] args) throws IOException {
        String linea;

        BufferedReader db = new BufferedReader(
                new InputStreamReader(App.class.getResource("/foto.txt").openStream()));

        while ((linea = db.readLine()) != null) {

            Busca(PARE, linea, PARE_NOEL);
            Busca(REN, linea, REN_NOM);
            Busca(FOLLET, linea, FOLLET_NOM);
            System.out.println("");

        }
    }

    private static void Busca(String pat, String linia, String text) {
        Pattern p = Pattern.compile(pat);
        Matcher m = p.matcher(linia);
        int count = 0;

        while (m.find()) {
            count++;
        }
        if (count != 0) {
            System.out.print(text + "(" + count + ") ");
        }
    }
}
